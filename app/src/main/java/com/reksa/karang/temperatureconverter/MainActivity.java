package com.reksa.karang.temperatureconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText etCelcius, etKelvin, etFahrenheit, etReamur;
    Button btnConvert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etCelcius = findViewById(R.id.et_celcius);
        etKelvin = findViewById(R.id.et_kelvin);
        etFahrenheit = findViewById(R.id.et_fahrenheit);
        etReamur = findViewById(R.id.et_reamur);
        btnConvert = findViewById(R.id.btn_convert);

        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int valCelcius = Integer.parseInt(etCelcius.getText().toString());
                    double valKelvin = valCelcius * 273.15;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
